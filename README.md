![image](https://i.imgur.com/aox4yVm.jpg)

# Warren Suitability Flow

This project is an alternative version for the Warren suitability flow. It is a technical challenge, part of the hiring proccess for the tech team.

Try the live version:
[https://warren-suitability.firebaseapp.com](https://warren-suitability.firebaseapp.com)

---

## Running the aplication

Install the dependencies:

`npm i` or `npm install`

Run the aplication locally

`npm start`

The application will be served at **http://localhost:3000**


## Other commands

If you want to run the tests

`npm test`

If you want to run the lint scan

`npm run lint`

IF you want to generate a production build

`npm run build`

## General information

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Having trouble while trying tu run the app? Have you found some embarassing bug? Please report to **guilherme.gades@gmail.com** :)
