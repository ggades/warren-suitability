// eslint-disable-next-line
import { configure } from 'enzyme';
// eslint-disable-next-line
import Adapter from 'enzyme-adapter-react-16';
import { fetch } from 'whatwg-fetch';

configure({ adapter: new Adapter() });

// Mock fetch to avoid external xhr requests
global.fetch = fetch;
