import DOMPurify from 'dompurify';
import Parser from 'html-react-parser';

const decodeMessage = message => {
  let decodedMessage;
  const pattern = /(\^[0-9]+\s?)/g;
  const hasEraseTag = message.value.match(/<erase>\(([^)]+)\)/g);

  if (message.owner === 'customer') return message.value;

  if (hasEraseTag) {
    const parsedValue = hasEraseTag[0].replace('<erase>', '');
    decodedMessage = message.value
      .replace(pattern, '')
      .replace(
        /<erase>\(([^)]+)\)/g,
        `<span class='erase'>${parsedValue}</span>`
      );
  } else {
    decodedMessage = message.value.replace(pattern, '');
  }

  return Parser(DOMPurify.sanitize(decodedMessage));
};

export default decodeMessage;
