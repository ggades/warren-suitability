import useActions from './use-actions';
import removeBlankSpaces from './removeBlankSpaces';
import inputType from './inputType';
import inputPlaceholder from './inputPlaceholder';
import isEmailValid from './isEmailValid';
import decodeMessage from './decodeMessage';

export {
  useActions,
  removeBlankSpaces,
  inputType,
  isEmailValid,
  inputPlaceholder,
  decodeMessage
};
