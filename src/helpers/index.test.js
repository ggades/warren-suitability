/* eslint-disable */
import React from 'react';
import Parser from 'html-react-parser';

import {
  decodeMessage,
  inputPlaceholder,
  inputType,
  isEmailValid,
  removeBlankSpaces
} from './index';

describe('Helpers', () => {

  it('should remove timestamps and tags from messages', () => {
    const message = {
      type: 'string', value: 'Você prefere verão ou inverno? ^1000 <erase>(ops... chat errado!) ^1000'
    };
    expect(decodeMessage(message)).not.toBeNull();
    expect(decodeMessage(message)).not.toBeUndefined();
    expect(decodeMessage(message)).toEqual(Parser('Você prefere verão ou inverno? <span className="erase">(ops... chat errado!)</span> '));
  });

  it('should return the correct input type based on the mask passed', () => {
    expect(inputType()).not.toBeNull();
    expect(inputType()).not.toBeUndefined();
    expect(inputType()).toBe('text');
    expect(inputType('name')).toBe('text');
    expect(inputType('integer')).toBe('number');
    expect(inputType('currency')).toBe('tel');
    expect(inputType('email')).toBe('email');
  });

  it('should return the correct placeholder based on the mask passed', () => {
    expect(inputPlaceholder()).not.toBeNull();
    expect(inputPlaceholder()).not.toBeUndefined();
    expect(inputPlaceholder()).toBe('Digite sua resposta');
    expect(inputPlaceholder('name')).toBe('Seu nome');
    expect(inputPlaceholder('integer')).toBe('Sua idade');
    expect(inputPlaceholder('currency')).toBe('R$ 0,00');
    expect(inputPlaceholder('email')).toBe('usuario@email.com');
  });

  it('should return if the email is valid or not', () => {
    expect(isEmailValid('yolo')).toBe(false);
    expect(isEmailValid('yolo.com.br')).toBe(false);
    expect(isEmailValid('yolo@warren')).toBe(false);
    expect(isEmailValid('yolo@.com.br')).toBe(false);
    expect(isEmailValid('@warren.com')).toBe(false);

    expect(isEmailValid('yolo@warren.com.br')).toBe(true);
    expect(isEmailValid('yolo@warren.com')).toBe(true);
  });

  it('should remove all blank spaces from a string', () => {
    expect(removeBlankSpaces('test test')).toBe('testtest');
    expect(removeBlankSpaces('test test ')).toBe('testtest');
    expect(removeBlankSpaces(' ')).toBe('');
  });
});
