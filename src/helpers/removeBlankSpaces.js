const removeBlankSpaces = value => value.replace(/\s/g, '');

export default removeBlankSpaces;
