const inputType = value => {
  let type;
  switch (value) {
    case 'name':
      type = 'text';
      break;
    case 'integer':
      type = 'number';
      break;
    case 'currency':
      type = 'tel';
      break;
    case 'email':
      type = 'email';
      break;
    default:
      type = 'text';
  }

  return type;
};

export default inputType;
