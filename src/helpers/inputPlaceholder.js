const inputPlaceholder = value => {
  let placeholder;
  switch (value) {
    case 'name':
      placeholder = 'Seu nome';
      break;
    case 'integer':
      placeholder = 'Sua idade';
      break;
    case 'currency':
      placeholder = 'R$ 0,00';
      break;
    case 'email':
      placeholder = 'usuario@email.com';
      break;
    default:
      placeholder = 'Digite sua resposta';
  }

  return placeholder;
};

export default inputPlaceholder;
