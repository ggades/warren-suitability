import {
  SET_MESSAGES,
  SET_SUITABILITY_STEP,
  SET_ANSWER,
  RESET_INPUTS,
  SET_ERROR,
  SET_LOADER,
  SET_USER_PROFILE
} from '../actions/session';

export const initialState = {
  isLoading: true,
  error: null,
  messages: [],
  inputs: [],
  step: null,
  answers: {},
  butons: [],
  userProfile: {}
};

export default (state = initialState, payload) => {
  switch (payload.type) {
    case SET_SUITABILITY_STEP:
      return {
        ...state,
        isLoading: false,
        messages: [...state.messages, ...payload.messages],
        inputs: payload.inputs,
        step: payload.step,
        buttons: payload.buttons
      };
    case SET_MESSAGES:
      return {
        ...state,
        messages: [...state.messages, ...payload.messages]
      };
    case SET_ANSWER:
      return {
        ...state,
        answers: { ...state.answers, ...payload.answer }
      };
    case SET_USER_PROFILE:
      return {
        ...state,
        userProfile: {
          name: payload.name,
          riskToleranceProfile: payload.riskToleranceProfile
        }
      };
    case RESET_INPUTS:
      return {
        ...state,
        buttons: [],
        inputs: []
      };
    case SET_LOADER:
      return {
        ...state,
        isLoading: payload.isLoading
      };
    case SET_ERROR:
      return {
        ...state,
        isLoading: false,
        error: payload.error
      };
    default:
      return state;
  }
};
