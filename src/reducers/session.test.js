import Session from './session';
import {
  SET_MESSAGES,
  SET_SUITABILITY_STEP,
  SET_ANSWER,
  RESET_INPUTS,
  SET_ERROR,
  SET_LOADER,
  SET_USER_PROFILE
} from '../actions/session';

describe('Session reducer', () => {
  it('fallback to a initial state', () => {
    expect(Session(undefined, { type: null })).toMatchSnapshot();
  });

  it('set message', () => {
    const payload = [{ type: 'string', value: "I'm your father" }];
    const success = Session(undefined, {
      type: SET_MESSAGES,
      messages: payload
    });

    expect(success).toHaveProperty('messages', payload);
  });

  it('set answer', () => {
    const payload = { question_one: "You can't handle the truth" };
    const success = Session(undefined, {
      type: SET_ANSWER,
      answer: payload
    });

    expect(success).toHaveProperty('answers', payload);
  });

  it('reset inputs', () => {
    const success = Session(undefined, {
      type: RESET_INPUTS
    });

    expect(success).toHaveProperty('inputs', []);
    expect(success).toHaveProperty('buttons', []);
  });

  it('set step', () => {
    const payload = {
      isLoading: false,
      messages: [
        { type: 'string', value: 'Never gonna give you up' },
        { type: 'string', value: 'Never gonna let you down' }
      ],
      inputs: [{ type: 'string', mask: 'name' }],
      step: 'never_gonna_run_around_and',
      buttons: []
    };
    const request = Session(undefined, { type: SET_LOADER, isLoading: true });
    const success = Session(undefined, {
      type: SET_SUITABILITY_STEP,
      ...payload
    });
    const failure = Session(undefined, {
      type: SET_ERROR,
      error: 'desert you'
    });

    expect(request).toHaveProperty('isLoading', true);

    expect(success).toHaveProperty('isLoading', false);
    expect(success).toHaveProperty('step', payload.step);
    expect(success).toHaveProperty('messages', payload.messages);
    expect(success).toHaveProperty('inputs', payload.inputs);
    expect(success).toHaveProperty('buttons', payload.buttons);

    expect(failure).toHaveProperty('error', 'desert you');
  });

  it('set user profile', () => {
    const payload = {
      name: 'Captain America',
      riskToleranceProfile: 'I understood that reference'
    };
    const request = Session(undefined, { type: SET_LOADER, isLoading: true });
    const success = Session(undefined, {
      type: SET_USER_PROFILE,
      ...payload
    });
    const failure = Session(undefined, {
      type: SET_ERROR,
      error: 'error message'
    });

    expect(request).toHaveProperty('isLoading', true);

    expect(success).toHaveProperty('userProfile.name', payload.name);
    expect(success).toHaveProperty(
      'userProfile.riskToleranceProfile',
      payload.riskToleranceProfile
    );

    expect(failure).toHaveProperty('error', 'error message');
  });
});
