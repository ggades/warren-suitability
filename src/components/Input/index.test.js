import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import { act } from 'react-dom/test-utils';
import { formatCurrency } from '@brazilian-utils/brazilian-utils';
import Input from './index';

describe('Input', () => {
  let props = {
    required: true,
    id: 'my-input-1',
    name: 'name',
    type: 'text',
    value: '',
    onChange: jest.fn(value => value),
    onKeyPress: jest.fn()
  };
  let wrapper = shallow(<Input {...props} />);

  it('renders without crashing', () => {
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });

  it('called onKeyPress', () => {
    wrapper.find('input').simulate('keypress', { key: 'Enter' });
    expect(props.onKeyPress).toHaveBeenCalled();
  });

  it('fill field', () => {
    act(() => {
      wrapper.find('input').simulate('change', {
        target: {
          value: 'test'
        }
      });
    });
    wrapper.update();
    expect(props.onChange).toBeCalled();
  });

  it('fill field with custom mask', () => {
    props = {
      required: true,
      id: 'my-input-2',
      name: 'price',
      type: 'tel',
      value: '',
      onChange: jest.fn(),
      onKeyPress: jest.fn(),
      setFormatter: jest.fn(formatCurrency)
    };
    wrapper = shallow(<Input {...props} />);
    act(() => {
      wrapper.find('input').simulate('change', {
        target: {
          value: '100'
        }
      });
    });
    wrapper.update();
    expect(props.onChange).toBeCalled();
  });
});
