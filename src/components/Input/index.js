import React, { useState, useEffect } from 'react';
import slugify from 'react-slugify';
import PropTypes from 'prop-types';
import { parseCurrency } from '@brazilian-utils/brazilian-utils';
import { removeBlankSpaces } from '../../helpers';
import './style.scss';

const Input = ({
  label,
  invalid,
  children,
  className,
  valid,
  inputClassName,
  setValue,
  setFormatter,
  customValidity,
  customValidityMessage,
  type,
  elementRef,
  handleKeyPress,
  ...props
}) => {
  const id = slugify(label);
  const [errorMsg, setErrorMsg] = useState();

  useEffect(() => {
    setErrorMsg(null);
  }, [label]);

  const resetErrors = target => {
    target.setCustomValidity('');
    target.classList.remove('invalid');
    setErrorMsg(null);
  };

  const validate = target => {
    if (!customValidityMessage) return;

    if (target.value.length === 0) {
      resetErrors(target);
      return;
    }

    if (customValidity) {
      target.setCustomValidity(
        customValidity(target.value) ? '' : customValidityMessage
      );
      setErrorMsg(target.validationMessage);
      return;
    }

    if (target.checkValidity()) {
      target.classList.remove('invalid');
      setErrorMsg(null);
    } else {
      target.classList.add('invalid');
      setErrorMsg(customValidityMessage);
    }
  };

  const fill = ({ value }) => {
    if (type === 'email') {
      setValue(removeBlankSpaces(value));
      return;
    }
    if (setFormatter) {
      if (type === 'tel') {
        const parsed = parseCurrency(value.replace('R$ ', ''));
        setValue(`R$ ${setFormatter(parsed)}`);
      } else {
        setValue(setFormatter(value));
      }
    } else {
      setValue(value);
    }
  };

  return (
    <div className={['form-group', className].join(' ')}>
      <input
        id={id}
        key={id}
        name={id}
        type={type}
        autoComplete="off"
        className={`form-control form-control-lg ${invalid ? 'is-invalid' : ''}
          ${valid ? 'is-valid' : ''} ${inputClassName}`}
        onChange={({ target }) => {
          target.classList.remove('invalid');
          fill(target);
        }}
        onBlur={({ target }) => {
          validate(target);
        }}
        ref={elementRef}
        onKeyPress={handleKeyPress}
        {...props}
      />
      {errorMsg && <div className="text-danger">{errorMsg}</div>}
      {children}
    </div>
  );
};

Input.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  inputClassName: PropTypes.string,
  label: PropTypes.node,
  invalid: PropTypes.bool,
  valid: PropTypes.bool,
  children: PropTypes.node,
  setValue: PropTypes.func,
  setFormatter: PropTypes.func,
  customValidity: PropTypes.func,
  customValidityMessage: PropTypes.string,
  elementRef: PropTypes.func,
  handleKeyPress: PropTypes.func
};

Input.defaultProps = {
  type: 'text',
  className: '',
  inputClassName: '',
  label: '',
  invalid: false,
  valid: false,
  children: null,
  setValue: () => {},
  setFormatter: null,
  customValidity: null,
  customValidityMessage: '',
  elementRef: () => {},
  handleKeyPress: () => {}
};

export default Input;
