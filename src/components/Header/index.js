import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

const Header = () => (
  <header className="header">
    <Link to="/suitability">
      <img src="../favicon.png" alt="Warren logo" width="50" height="50" />
    </Link>
    <h1>Perfil de investidor</h1>
  </header>
);

export default Header;
