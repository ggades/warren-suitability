import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Header from './index';

describe('Header', () => {
  it('renders without crashing', () => {
    const component = shallow(
      <BrowserRouter>
        <Header />
      </BrowserRouter>
    );
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
