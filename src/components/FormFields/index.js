import React from 'react';
import PropTypes from 'prop-types';
import { formatCurrency } from '@brazilian-utils/brazilian-utils';
import { inputType, isEmailValid, inputPlaceholder } from '../../helpers';
import Input from '../Input';

const FormFields = ({ inputs, step, setValue, value }) => {
  return inputs.map((input, i) => {
    return input.type ? (
      <Input
        required
        key={`input-${i}`} // eslint-disable-line react/no-array-index-key
        name={step}
        maxLength="50"
        type={inputType(input.mask)}
        placeholder={inputPlaceholder(input.mask)}
        setValue={setValue}
        value={value}
        setFormatter={input.mask === 'currency' ? formatCurrency : null}
        customValidity={input.mask === 'email' ? isEmailValid : null}
        customValidityMessage={
          input.mask === 'email' ? 'Preencha com um e-mail válido' : ''
        }
      />
    ) : (
      <label
        key={`button-${i}`} // eslint-disable-line react/no-array-index-key
        className="button-label"
        htmlFor={`button-${i}`}
        title={input.label.title}
      >
        <Input
          required
          id={`button-${i}`}
          name={step}
          type="radio"
          setValue={setValue}
          value={input.value}
          defaultChecked={false}
        >
          <div
            className={`inner-button${
              input.label.title.length >= 15 ? ' small-font' : ''
            }`}
          >
            {input.label.title}
          </div>
        </Input>
      </label>
    );
  });
};

FormFields.defaultProps = {
  step: null,
  inputs: []
};

FormFields.propTypes = {
  step: PropTypes.string,
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  inputs: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object]))
};

export default FormFields;
