import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import FormFields from './index';

describe('Form', () => {
  const props = {
    step: 'watch_your',
    value: '',
    setValue: jest.fn(),
    inputs: [
      {
        mask: 'name',
        type: 'string'
      }
    ]
  };
  const wrapper = shallow(<FormFields {...props} />);

  it('renders without crashing', () => {
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
