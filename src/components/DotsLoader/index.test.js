import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import DotsLoader from './index';

describe('DotsLoader', () => {
  it('renders without crashing', () => {
    const component = shallow(<DotsLoader />);
    expect(shallowToJson(component)).toMatchSnapshot();
  });
});
