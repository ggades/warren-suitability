import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const DotsLoader = ({ contrast }) => (
  <div className={`loading-dots ${contrast}`}>
    <div className="loading-dots--dot" />
    <div className="loading-dots--dot" />
    <div className="loading-dots--dot" />
  </div>
);

DotsLoader.propTypes = {
  contrast: PropTypes.oneOf(['light', 'dark'])
};

DotsLoader.defaultProps = {
  contrast: 'light'
};

export default DotsLoader;
