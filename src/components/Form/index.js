import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './style.scss';

const Form = ({ onSubmit, customClass, ...props }) => {
  const [wasValidated, setWasValidated] = useState(false);

  useEffect(() => setWasValidated(false), [onSubmit]);

  const validaTeForm = ev => {
    const form = ev.target;

    ev.preventDefault();
    ev.stopPropagation();

    if (form.checkValidity()) {
      onSubmit(ev);
    } else {
      form.querySelector(':invalid').focus();
      const inputsInvalid = form.querySelectorAll(':invalid');

      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < inputsInvalid.length; ++i) {
        if (inputsInvalid[i].type === 'password') {
          inputsInvalid[i].parentNode.classList.add('invalid');
        } else {
          inputsInvalid[i].classList.add('invalid');
        }
      }
    }
    setWasValidated(true);
  };

  return (
    <form
      className={`form ${customClass} ${wasValidated ? 'was-validated' : ''}`}
      noValidate
      onSubmit={validaTeForm}
      autoComplete="false"
      {...props}
    />
  );
};

Form.propTypes = {
  onSubmit: PropTypes.func,
  customClass: PropTypes.string
};

Form.defaultProps = {
  onSubmit: () => {},
  customClass: ''
};

export default Form;
