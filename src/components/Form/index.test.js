import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import { act } from 'react-dom/test-utils';
import Form from './index';
import Input from '../Input';

describe('Form', () => {
  const props = {
    onSubmit: jest.fn()
  };
  let wrapper = shallow(
    <Form {...props}>
      <Input required id="my-input-2" name="name" type="text" value="" />
      <button type="submit">Submit</button>
    </Form>
  );

  beforeEach(() => {
    wrapper = shallow(
      <Form {...props}>
        <Input required id="my-input-2" name="name" type="text" value="" />
        <button type="submit">Submit</button>
      </Form>
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders without crashing', () => {
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });

  it('submit form', () => {
    act(() => {
      const mockedEvent = {
        target: {
          checkValidity: jest.fn(() => true)
        },
        stopPropagation: jest.fn(),
        preventDefault: jest.fn()
      };
      wrapper.find(Input).simulate('change', 'never gonna give you up');
      wrapper.find('form').simulate('submit', mockedEvent);
    });
    wrapper.update();
    expect(props.onSubmit).toHaveBeenCalled();
  });
});
