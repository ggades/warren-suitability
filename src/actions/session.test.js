import { sendConversationMessage, finishConversation } from './session';

describe('Session actions', () => {
  let dispatch = jest.fn();

  beforeEach(() => {
    dispatch = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('start a conversation', () => {
    sendConversationMessage({ id: null })(dispatch);

    expect(dispatch).toBeCalled();
    expect(dispatch.mock.calls[0][0]).toHaveProperty('type', 'SET_LOADER');
  });

  it('send a conversation message', () => {
    sendConversationMessage({
      id: 'question_isretired',
      answers: {
        question_name: 'yolo',
        question_age: 30,
        question_isretired: 'Y'
      }
    })(dispatch);

    expect(dispatch).toBeCalled();
    expect(dispatch.mock.calls[0][0]).toHaveProperty('type', 'SET_LOADER');
  });

  it('finish conversation', () => {
    finishConversation({
      answers: {
        question_name: 'test',
        question_age: 30,
        question_interest: 'A',
        question_preference: 'A',
        question_income: '8001',
        question_read: 'A',
        question_investment_experience: 'P',
        question_market: 'P',
        question_learning: 'P',
        question_email: 'user@email.com'
      }
    })(dispatch);

    expect(dispatch).toBeCalled();
    expect(dispatch.mock.calls[0][0]).toHaveProperty('type', 'SET_LOADER');
  });
});
