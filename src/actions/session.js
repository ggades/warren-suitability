import axios from 'axios';

// request config
const API = process.env.REACT_APP_API;

export const SET_SUITABILITY_STEP = 'SET_SUITABILITY_STEP';
export const SET_MESSAGES = 'SET_MESSAGES';
export const SET_ANSWER = 'SET_ANSWER';
export const SET_USER_PROFILE = 'SET_USER_PROFILE';
export const RESET_INPUTS = 'RESET_INPUTS';
export const SET_LOADER = 'SET_LOADER';
export const SET_ERROR = 'SET_ERROR';

export const setMessages = messages => dispatch => {
  dispatch({ type: SET_MESSAGES, messages });
};

export const setAnswer = answer => dispatch => {
  dispatch({ type: SET_ANSWER, answer });
};

export const resetInputs = () => dispatch => {
  dispatch({ type: RESET_INPUTS });
};

export const sendConversationMessage = (payload = {}) => dispatch => {
  dispatch({ type: SET_LOADER, isLoading: true });
  axios({
    method: 'POST',
    url: API.concat('/conversation/message'),
    data: {
      context: 'suitability',
      ...payload
    }
  })
    .then(response => {
      const { data } = response;
      dispatch({
        type: SET_SUITABILITY_STEP,
        messages: [data.messages],
        step: data.id,
        inputs: data.inputs,
        buttons: data.buttons
      });
    })
    .catch(error => {
      const requestError =
        'Beep boop, error while trying to send a conversation message';
      console.error(
        '[ERROR][Actions] sendConversationMessage ',
        requestError,
        error
      );
      dispatch({ type: SET_ERROR, error: requestError });
    });
};

export const finishConversation = payload => dispatch => {
  dispatch({ type: SET_LOADER, isLoading: true });
  axios({
    method: 'POST',
    url: API.concat('/suitability/finish'),
    data: {
      answers: { ...payload }
    }
  })
    .then(response => {
      const { data } = response;
      dispatch({
        type: SET_USER_PROFILE,
        name: data.user.name,
        riskToleranceProfile: data.user.investmentProfile.riskToleranceProfile
      });
    })
    .catch(error => {
      const requestError = 'Error while trying to finish the conversation';
      console.error(
        '[ERROR][Actions] finishConversation ',
        requestError,
        error
      );
      dispatch({ type: SET_ERROR, error: requestError });
    });
};
