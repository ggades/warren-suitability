import React from 'react';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';

// Containers
import Suitability from '../containers/Suitability';
import Profile from '../containers/Profile';

const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/suitability" component={Suitability} />
      <Route exact path="/suitability/my-profile" component={Profile} />
      <Redirect to="/suitability" />
    </Switch>
  </BrowserRouter>
);

export default AppRouter;
