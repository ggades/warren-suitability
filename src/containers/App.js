import React from 'react';
import StoreProvider from './StoreProvider';
import AppRouter from '../routes';

const App = () => {
  return (
    <StoreProvider>
      <AppRouter />
    </StoreProvider>
  );
};

export default App;
