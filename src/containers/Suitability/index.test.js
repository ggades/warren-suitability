import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import SuitabilityContainer from './index';

describe('SuitabilityContainer', () => {
  let container = null;
  const props = {
    isLoading: false,
    sendConversationMessage: jest.fn(),
    setMessages: jest.fn(),
    setAnswer: jest.fn(),
    messages: [],
    step: null,
    inputs: [],
    answers: {},
    buttons: [
      {
        value: 'A',
        label: { title: 'I like it' }
      },
      {
        value: 'P',
        label: { title: 'I dont like it' }
      }
    ],
    resetInputs: jest.fn(),
    finishConversation: jest.fn(),
    userProfile: {},
    history: {
      push: jest.fn()
    }
  };

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  // Couldn't test chat HTML due to the Typist component (was rendered empty in tests)
  it('renders without crashing', () => {
    act(() => {
      render(
        <BrowserRouter>
          <SuitabilityContainer {...props} />
        </BrowserRouter>,
        container
      );
    });

    expect(container.textContent).toBe(
      'Perfil de investidorI like itI dont like itOk'
    );
    expect(container.querySelector('.input-container').textContent).toBe(
      'I like itI dont like itOk'
    );
  });
});
