/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-array-index-key */
import React, { useContext, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import Typist from 'react-typist';
import { parseCurrency } from '@brazilian-utils/brazilian-utils';
import Form from '../../components/Form';
import { decodeMessage } from '../../helpers';
import Header from '../../components/Header';
import DotsLoader from '../../components/DotsLoader';
import FormFields from '../../components/FormFields';
import { SessionContext } from '../StoreProvider';
import './style.scss';

const SuitabilityContainer = ({
  isLoading,
  sendConversationMessage,
  setMessages,
  setAnswer,
  messages,
  step,
  inputs,
  answers,
  buttons,
  resetInputs,
  finishConversation,
  userProfile,
  history
}) => {
  const [value, setValue] = useState('');
  const [inputContainerVisible, setInputContainerVisible] = useState(false);
  const submitRef = useRef(null);

  useEffect(() => {
    sendConversationMessage();
  }, []);

  useEffect(() => {
    if (step === 'final') finishConversation(answers);
  }, [step]);

  useEffect(() => {
    if (userProfile.name) history.push('/suitability/my-profile');
  }, [userProfile]);

  useEffect(() => {
    // Autosubmit form when user choose an option
    if (value !== '' && buttons.length) {
      submitRef.current.click();
    }
  }, [value]);

  const buildBotMessages = arr =>
    arr.map((message, i) => {
      return (
        <div key={i}>
          <div className="message bot">{decodeMessage(message)}</div>
          {i + 1 !== arr.length ? <Typist.Delay ms={1000} /> : <div />}
        </div>
      );
    });

  // Render chat conversation based on messages array
  const renderChat = () => {
    if (!messages) return null;

    // Check for MS intervals and <erase> tag through decodeMessage() helper
    return messages.map((message, i) => {
      return (
        <Typist
          avgTypingDelay={10}
          stdTypingDelay={10}
          key={i}
          onLineTyped={() => window.scrollTo(0, document.body.scrollHeight)} // Autoscroll page after each line typed
          onTypingDone={
            message.length >= 1
              ? () => {
                  setInputContainerVisible(!!(inputs.length || buttons.length));
                }
              : () => {}
          }
          cursor={{
            show: false
          }}
        >
          {message.length >= 1 ? (
            buildBotMessages(message)
          ) : (
            <div key={i} className={`message ${message.owner || 'bot'}`}>
              {decodeMessage(message)}
            </div>
          )}
        </Typist>
      );
    });
  };

  // Render dynamic inputs based on the conversation response
  const renderInputs = () => {
    if (!inputs && !buttons) return null;

    return (
      <FormFields
        inputs={inputs.length ? inputs : buttons}
        step={step}
        value={value}
        setValue={setValue}
      />
    );
  };

  const findAliasMessage = valueToFind => {
    if (!buttons) return value;

    const buttonLabelFound = buttons.find(
      button => button.value === valueToFind
    );
    const label =
      buttonLabelFound && buttonLabelFound.label
        ? buttonLabelFound.label.title
        : value;

    return label;
  };

  const onSubmit = () => {
    const answer =
      step === 'question_income'
        ? { [step]: parseCurrency(value) }
        : { [step]: value };

    // Update the chat with the customer answer, findAliasMessage() is used to print a formatted answer. Ex: R$ 100,00 instead of 100
    setMessages([
      { type: 'string', owner: 'customer', value: findAliasMessage(value) }
    ]);

    // Set the answer on Context. It will be use to send the final step with all answers
    setAnswer(answer);

    // Send the actual answer to the API
    sendConversationMessage({
      id: step,
      answers: {
        ...answers,
        ...answer
      }
    });

    // Hide input container area
    setInputContainerVisible(false);

    // Reset value state and inputs/buttons to avoid radiobutton autocheck when loading a new array of options
    setValue('');
    resetInputs();
  };

  return (
    <>
      <Header />
      <div className="default-template suitablity-container">
        <div className="content">
          <div className="chat-container">
            {renderChat()}
            {isLoading && <DotsLoader />}
          </div>
          <div
            className={`input-container${
              inputContainerVisible ? ' visible' : ''
            }`}
          >
            <Form
              {...{
                onSubmit,
                customClass: buttons.length ? 'align-center' : ''
              }}
            >
              {renderInputs()}
              <button
                className={`btn${buttons.length ? ' hidden' : ''}`}
                type="submit"
                ref={submitRef}
                disabled={isLoading || value === ''}
              >
                Ok
              </button>
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

SuitabilityContainer.defaultProps = {
  messages: [],
  step: null,
  inputs: [],
  buttons: [],
  answers: {},
  userProfile: {}
};

SuitabilityContainer.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  sendConversationMessage: PropTypes.func.isRequired,
  messages: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.array])
  ),
  step: PropTypes.string,
  inputs: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])),
  setMessages: PropTypes.func.isRequired,
  setAnswer: PropTypes.func.isRequired,
  buttons: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])),
  answers: PropTypes.shape({ question_name: PropTypes.string }),
  resetInputs: PropTypes.func.isRequired,
  finishConversation: PropTypes.func.isRequired,
  userProfile: PropTypes.shape({ name: PropTypes.string }),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};

export default props => (
  <SuitabilityContainer {...useContext(SessionContext)} {...props} />
);
