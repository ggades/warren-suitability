import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import Header from '../../components/Header';
import { SessionContext } from '../StoreProvider';
import './style.scss';

const ProfileContainer = ({ userProfile, history }) => {
  const scrollToTop = () => {
    const c = document.documentElement.scrollTop || document.body.scrollTop;
    if (c > 0) {
      window.requestAnimationFrame(scrollToTop);
      window.scrollTo(0, c - c / 8);
    }
  };

  // Redirect to the flow if there isn't a profile set on the context
  useEffect(() => {
    if (!userProfile.name) history.push('suitability');
    scrollToTop();
  }, [userProfile, history]); /* eslint-disable-line */

  return (
    <>
      <Header />
      <div className="default-template profile-container">
        <div className="content">
          <div className="profile-description">
            <div className="middle">
              <p>{`Muito bem ${userProfile.name}.`}</p>
              <p>O seu perfil é...</p>
              <p className="profile-name fade-in">
                {userProfile.riskToleranceProfile}
              </p>
            </div>
          </div>
          <div className="card">
            <h2>Investimentos sob medida para o seu momento</h2>
            <p>
              Você já teve o primeiro contato com o mundo dos investimentos e já
              conhece os principais produtos. Quer estar por dentro do que está
              acontecendo com os seus rendimentos com informações claras e
              transparentes.
            </p>
            <a
              href="https://warren.com.br/app/#/v3"
              title="Acessar a plataforma Warren"
              className="btn"
            >
              Acessar a plataforma
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

ProfileContainer.defaultProps = {
  userProfile: {}
};

ProfileContainer.propTypes = {
  userProfile: PropTypes.shape({
    name: PropTypes.string,
    riskToleranceProfile: PropTypes.string
  }),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired
};

export default props => (
  <ProfileContainer {...useContext(SessionContext)} {...props} />
);
