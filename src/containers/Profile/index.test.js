import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import ProfileContainer from './index';

describe('ProfileContainer', () => {
  let container = null;
  const mockHistory = {
    push: jest.fn()
  };

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it('renders without crashing', () => {
    const expected =
      'Perfil de investidorMuito bem Yolo.O seu perfil é...Too risky brotherInvestimentos sob medida para o seu momentoVocê já teve o primeiro contato com o mundo dos investimentos e já conhece os principais produtos. Quer estar por dentro do que está acontecendo com os seus rendimentos com informações claras e transparentes.Acessar a plataforma';
    act(() => {
      render(
        <BrowserRouter>
          <ProfileContainer
            history={mockHistory}
            userProfile={{
              name: 'Yolo',
              riskToleranceProfile: 'Too risky brother'
            }}
          />
        </BrowserRouter>,
        container
      );
    });
    expect(container.textContent).toBe(expected);
  });
});
