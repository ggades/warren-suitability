import React, { useReducer, createContext } from 'react';
import PropTypes from 'prop-types';
import useActions from '../helpers/use-actions';
import sessionReducer, {
  initialState as sessionInitialState
} from '../reducers/session';
import * as sessionActions from '../actions/session';

export const SessionContext = createContext();

const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(sessionReducer, sessionInitialState);
  const boundActions = useActions(sessionActions, dispatch);

  return (
    <SessionContext.Provider value={{ ...boundActions, ...state }}>
      {children}
    </SessionContext.Provider>
  );
};

StoreProvider.propTypes = {
  children: PropTypes.node
};

StoreProvider.defaultProps = {
  children: null
};

export default StoreProvider;
